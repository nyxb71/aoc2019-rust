mod rom;

extern crate derive_more;
use derive_more::{Add, AddAssign, Display, From, Mul};

use std::ops::Index;
use std::ops::IndexMut;

#[derive(From, Add, AddAssign, Mul, Display, PartialEq, Eq, Copy, Clone)]
struct Address(i32);

impl std::convert::From<Memory> for Address {
    fn from(mem: Memory) -> Self {
        Address(mem.0)
    }
}

impl std::convert::From<usize> for Address {
    fn from(u: usize) -> Self {
        Address(u as i32)
    }
}

#[derive(From, Add, AddAssign, Mul, Display, PartialEq, Eq, Copy, Clone)]
#[mul(forward)]
struct Value(i32);

#[derive(From, Add, AddAssign, Mul, Display, PartialEq, Eq, Copy, Clone)]
#[mul(forward)]
struct Memory(i32);

impl std::convert::From<Address> for Memory {
    fn from(addr: Address) -> Self {
        Memory(addr.0)
    }
}

impl std::convert::From<Value> for Memory {
    fn from(val: Value) -> Self {
        Memory(val.0)
    }
}

type RAM = Vec<Memory>;

impl Index<Address> for RAM {
    type Output = Memory;

    fn index(&self, address: Address) -> &Memory {
        &self[address.0 as usize]
    }
}

impl IndexMut<Address> for RAM {
    fn index_mut(&mut self, address: Address) -> &mut Memory {
        &mut self[address.0 as usize]
    }
}

fn add(a: Address, b: Address, dest: Address, ram: &mut RAM) {
    ram[dest] = ram[a] + ram[b];
}

fn mult(a: Address, b: Address, dest: Address, ram: &mut RAM) {
    ram[dest] = ram[a] * ram[b];
}

fn mov(val: Value, dest: Address, ram: &mut RAM) {
    ram[dest] = Memory::from(val);
}

fn read(dest: Address, ram: &mut RAM) {}

fn print(a: Address, ram: &mut RAM) {
    println!("{}", ram[a]);
}

enum ParamMode {
    Immediate,
    Position,
}

#[derive(Display)]
enum Opcodes {
    Add,
    Mult,
    Read,
    Print,
    Halt,
}

struct Opcode {
    op: Opcodes,
    params: Vec<Memory>,
    param_modes: Vec<ParamMode>,
    length: usize,
}

fn digits_big_endian_to_u32(digits: &[u32]) -> u32 {
    let mut res = 0;
    for (i, e) in digits.iter().enumerate() {
        res += e * (i as u32 * 10);
    }

    res
}

fn parse_opcode(pc: Address, ram: &RAM) -> Option<Opcodes> {
    let op_digits: Vec<u32> = ram[pc]
        .0
        .to_string()
        .chars()
        .rev()
        .filter_map(|x| x.to_digit(10))
        .collect();

    println!("{} -> {:?}", pc, op_digits);

    match op_digits.len() {
        1 => match op_digits[0] {
            1 => Some(Opcodes::Add),
            2 => Some(Opcodes::Mult),
            3 => Some(Opcodes::Read),
            4 => Some(Opcodes::Print),
            _ => None,
        },
        2 => match digits_big_endian_to_u32(op_digits.as_slice()) {
            99 => Some(Opcodes::Halt),
            _ => None,
        },
        4 => match digits_big_endian_to_u32(&op_digits[..2]) {
            1 => Some(Opcodes::Add),
            2 => Some(Opcodes::Mult),
            _ => None,
        },
        _ => None,
    }
}

fn run_machine(rom: RAM) {
    let mut ram = rom.clone();
    let mut pc: usize = 0;

    while pc < ram.len() {
        let diff = parse_opcode(Address::from(pc), &ram);

        match diff {
            Some(d) => {
                println!("{} {}", pc, d);
                pc += 4;
            }
            None => {
                println!("{} None", pc);
                pc += 1;
            }
        }

        // pc += diff;
        // let op = ram[Address::from(pc)];

        // let a = ram[Address::from(pc) + Address(1)];
        // let b = ram[Address::from(pc) + Address(2)];
        // let dest = ram[Address::from(pc) + Address(3)];

        // match op.0 {
        //     1 => {
        //         add(
        //             Address::from(a),
        //             Address::from(b),
        //             Address::from(dest),
        //             &mut ram,
        //         );
        //         pc += 4;
        //     }
        //     2 => {
        //         mult(
        //             Address::from(a),
        //             Address::from(b),
        //             Address::from(dest),
        //             &mut ram,
        //         );
        //         pc += 4;
        //     }
        //     3 => {
        //         mov(Value::from(0), Address::from(dest), &mut ram);
        //         pc += 2;
        //     }
        //     4 => {
        //         print(Address::from(a), &mut ram);
        //     }
        //     99 => {
        //         break;
        //     }
        //     _ => {
        //         println!("invalid opcode {} at {}", op, pc);
        //         break;
        //     }
        // }
    }

    println!("{}", ram[0]);
}

fn main() {
    let rom: RAM = rom::get_rom_bytes().iter().map(|v| Memory(*v)).collect();
    run_machine(rom);
}
