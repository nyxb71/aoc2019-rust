fn get_digits(pass: i32) -> Vec<i8> {
    let mut p = pass;
    let mut digits: Vec<i8> = vec![];
    while p > 0 {
        digits.push((p % 10) as i8);
        p /= 10;
    }
    digits.reverse();
    digits
}

fn is_ascending(digits: &[i8]) -> bool {
    for i in 1..digits.len() {
        if digits[i] < digits[i - 1] {
            return false;
        }
    }

    true
}

fn group_adjacent_duplicate_digits(digits: &[i8]) -> Vec<Vec<i8>> {
    let mut groups: Vec<Vec<i8>> = vec![];

    let mut curret_group: Vec<i8> = vec![];
    for digit in digits {
        if curret_group.is_empty() {
            curret_group.push(*digit);
            continue;
        }

        if *digit == *curret_group.last().unwrap() {
            curret_group.push(*digit)
        } else {
            groups.push(curret_group.clone());
            curret_group.clear();
            curret_group.push(*digit)
        }
    }
    groups.push(curret_group);

    groups
}

fn is_valid_password(pass: i32) -> bool {
    let digits = get_digits(pass);

    if !is_ascending(&digits) {
        return false;
    }

    let groups = group_adjacent_duplicate_digits(&digits);

    if groups.iter().filter(|x| x.len() == 2).count() == 0 {
        return false;
    }

    true
}

fn main() {
    const START: i32 = 367_479;
    const END: i32 = 893_698;

    let mut count = 0;
    for i in START..=END {
        if is_valid_password(i) {
            count += 1;
        }
    }

    println!("{}", count);
}
